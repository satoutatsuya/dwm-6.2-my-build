/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const char panel[][20]       = { "xfce4-panel", "Xfce4-panel" }; /* name & cls of panel win */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 3;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonwheel       = 0;
static const char *fonts[]          = { "Fira Code:style=Bold:size=11:antialias=true:autohint=true", 
										"JoyPixels:pixelsize=16:antialias=true:autohint=true", 
										"Material Design Icons:style=Regular:pixelsize=16", 
										"FontAwesome:style=Bold:pixelsize=16" };
static const char dmenufont[]       = "Roboto mono:size=12";
static const char col_gray1[]       = "#1a1b26"; 
static const char col_gray2[]       = "#7da6ff";
static const char col_gray3[]       = "#98c1d9";
static const char col_gray4[]       = "#a9b1d6";
static const char col_cyan[]        = "#ff7a93";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, col_gray1, 	col_gray1},
	[SchemeSel]  = { col_gray1, col_gray2,  col_cyan},
	[SchemeTitle]= { col_gray1, col_gray2,  col_cyan},
};

/* tagging */
static const char *tags[] = { "󰈹", "󰞷", "󰥟", "󱔗", "󰭹", "󰇎", "󱕵" };
//static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9"};
//static const char *tags[] = { "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣"};
//static const char *tags[] = { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class							instance    title						tags mask     	isfloating     monitor */
	{ "Evolution",						NULL,       NULL,						1<<4 ,  	    0,           -1 }, //Evolution opens in 5th tag
	{ "firefox",						NULL,       "Picture-in-Picture",       ~0,  	    	1,           -1 }, //Firefox P-I-P floats and appears on all tags
	{ "Microsoft Teams - Preview", 		NULL,       NULL,						1<<4,   	    0,           -1 }, //teams opens in 5th tag
	{ "Signal",							NULL,       NULL,						1<<4,   	    0,           -1 }, //signal opens in 5th tag
	{ "ij-ImageJ",						NULL,       NULL,						NULL ,  	    1,           -1 }, //makes imagej floating
	{ "calibre",						NULL,       "E-book viewer",       		NULL ,  	    1,           -1 }, //makes calibre ebook viewer floating
	{ "smplayer",						NULL,       NULL,						NULL ,  	    1,           -1 }, //makes smplayer floating
	{ "Galculator",						NULL,       NULL,						NULL ,  	    1,           -1 }, //makes galculator floating
	{ "Gnome-calculator",				NULL,       NULL,						NULL ,  	    1,           -1 }, //makes gnome-calculator floating
	{ "gnome-calculator",				NULL,       NULL,						NULL ,  	    1,           -1 }, //makes gnome-calculator floating
	{ "calculator",						NULL,       NULL,						NULL ,  	    1,           -1 }, //makes gnome-calculator floating
	{ "Pavucontrol",					NULL,       NULL,						NULL ,  	    1,           -1 }, //makes pavucontrol floating
	{ "qt5ct",	  						NULL,       NULL,						NULL ,  	    1,           -1 }, //makes qt5ct floating
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

#include "layouts.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "󰙀",      tile },    /* first entry is default */
	{ "󰘷",      NULL },    /* no layout function means floating behavior */
	{ "󰁌",      monocle },
	{ "",      deck },
	{ "󰟸",      bstack },
	{ "󰛻",      bstackhoriz },
	{ "󰕰",      grid }, //grid layout
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define BRIUP  XF86XK_MonBrightnessUp 
#define BRIDWN XF86XK_MonBrightnessDown
#define VOLUP  XF86XK_AudioRaiseVolume
#define VOLDWN XF86XK_AudioLowerVolume
#define MUTE   XF86XK_AudioMute
#define PLAY   XF86XK_AudioPlay
#define PAUSE  XF86XK_AudioStop
#define PREV   XF86XK_AudioPrev
#define NEXT   XF86XK_AudioNext
#define CALC   XF86XK_Calculator
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/fish", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] 					= "1"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] 				= { "dmenu_run", "-c", "-i", "-l", "10"};
static const char *dmenudruncmd[] 			= { "dmenu_drun", NULL};
static const char *filesearchcmd[] 			= {	"dmenu-file-search", NULL};
static const char *termcmd[]  				= { "kitty", NULL };
static const char *firefoxcmd[]  			= { "firefox", NULL };//firefox command 
static const char *emailcmd[]  				= { "evolution", NULL };//Evolution
static const char *filemancmd[]  			= { "nautilus", NULL };//graphical file manager command 
static const char *vifmcmd[]  				= { "kitty", "-e", "vifm" };//cli file manager command 
static const char *vimcmd[]  				= { "neovide", "--multigrid", NULL };//editor command 
static const char *brightupcmd[]  			= { "brightnessctl", "s", "5%+" };//brightness up command 
static const char *brightdowncmd[]  		= { "brightnessctl", "s", "5%-" };//brightness down command 
static const char *volupcmd[]  				= { "pactl", "set-sink-volume", "0", "+5%", NULL };//Volume up command 
static const char *voldowncmd[]  			= { "pactl", "set-sink-volume", "0", "-5%", NULL };//Volume down command 
static const char *volmutecmd[]  			= { "pactl", "set-sink-mute", "0", "toggle", NULL };//Volume mute command 
static const char *audioplaypausecmd[]  	= { "playerctl", "-a", "play-pause" };//Audio play-pause toggle
static const char *audionextcmd[]  			= { "playerctl", "-a", "next" };//Audio next track
static const char *audioprevcmd[]  			= { "playerctl", "-a", "previous" };//Audio previous track
static const char *slockcmd[]  				= { "slock", NULL };//slock command
static const char *i3lockcmd[]  			= { "i3lock", "-c", "000000"  };//i3lock command
static const char *scrotcmd[] 				= { "xfce4-screenshooter"}; //screenshot command
static const char *powercmd[] 				= { "shutdown_script", NULL}; //power or shutdown options command
static const char *taskmancmd[] 			= { "kitty", "-e", "htop"}; //task manager command
static const char *setmonitorcmd[] 			= { "multi_monitor.sh"}; //sets monitor and wallpaper
static const char *connectivitycmd[] 		= { "connectivity", NULL}; //command to control wifi and bluetooth
static const char *calculatorcmd[] 			= { "gnome-calculator", NULL}; //command to open calculator
static const char *websearchcmd[] 			= { "web-search", NULL}; //command to do web search using dmenu and firefox
static const char *roficmd[]				= { "rofi", "-show", "drun", NULL}; //command to open rofi drun
//static const char *krunnercmd[] 			= {"krunner", NULL}; //command to launch krunner

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = dmenucmd } },
	{ MODKEY,             			XK_space,  spawn,          {.v = roficmd } },
//	{ MODKEY,             			XK_space,  spawn,          {.v = dmenudruncmd } },
	{ MODKEY,             			XK_slash,  spawn,          {.v = filesearchcmd } },
	{ MODKEY,            			XK_Return, spawn,          {.v = termcmd } },
	{ ControlMask|MODKEY,    		XK_w, 	   spawn,          {.v = firefoxcmd } },//launch firefox
	{ ShiftMask|MODKEY,     		XK_slash,  spawn,          {.v = websearchcmd } },//launch web-search script
	{ ControlMask|MODKEY,    		XK_e, 	   spawn,          {.v = emailcmd } },//launch evolution email
	{ MODKEY|ShiftMask,	    		XK_f, 	   spawn,          {.v = vifmcmd } },//launch vifm
	{ MODKEY|ShiftMask,	    		XK_v, 	   spawn,          {.v = vimcmd } },//launch vim
	{ ControlMask|MODKEY,   	 	XK_f, 	   spawn,          {.v = filemancmd } },//launch file manager
	{ NULL,				    		BRIUP, 	   spawn,          {.v = brightupcmd } },//increase brightness
	{ NULL,				    		BRIDWN,	   spawn,          {.v = brightdowncmd } },//decrease brightness
	{ NULL,				    		VOLUP,	   spawn,          {.v = volupcmd } },//increase volume
	{ NULL,			    			VOLDWN,	   spawn,          {.v = voldowncmd } },//decrease volume
	{ NULL,			    			MUTE,	   spawn,          {.v = volmutecmd } },//mute volume
	{ NULL,			    			PLAY,	   spawn,          {.v = audioplaypausecmd } },//audio play-pause toggle
	{ NULL,			    			NEXT,	   spawn,          {.v = audionextcmd } },//audio next track
	{ NULL,			    			PREV,	   spawn,          {.v = audioprevcmd } },//audio previous track
	{ NULL,			    			CALC,	   spawn,          {.v = calculatorcmd } },//open calculator
	{ ALTKEY,			    		XK_l,	   spawn,          {.v = slockcmd } },//lock screen
	{ MODKEY|ShiftMask,	    		XK_s,	   spawn,          {.v = scrotcmd } },//screenshot
	{ MODKEY|ShiftMask,	    		XK_c,	   spawn,          {.v = connectivitycmd } },//wifi and bluetooth options
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },//Change focus b/w windows
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },//Change focus b/w windows
	{ MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY|ALTKEY,             	XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ALTKEY,             	XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_h, 	   zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,            			XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_s,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,	            XK_t,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ControlMask,           XK_t,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_g,      setlayout,      {.v = &layouts[6]} },//switch to grid layout
//	{ MODKEY,                       XK_Tab,    setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
	{ MODKEY,                       XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
	{ MODKEY,                       XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
	{ MODKEY,                       XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ MODKEY|ShiftMask,             XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
	{ MODKEY|ShiftMask,             XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
	{ MODKEY|ShiftMask,             XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
	{ MODKEY|ShiftMask,             XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },
	{ MODKEY|ControlMask,           XK_Up,     moveresizeedge, {.v = "t"} },
	{ MODKEY|ControlMask,           XK_Down,   moveresizeedge, {.v = "b"} },
	{ MODKEY|ControlMask,           XK_Left,   moveresizeedge, {.v = "l"} },
	{ MODKEY|ControlMask,           XK_Right,  moveresizeedge, {.v = "r"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Up,     moveresizeedge, {.v = "T"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Down,   moveresizeedge, {.v = "B"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Left,   moveresizeedge, {.v = "L"} },
	{ MODKEY|ControlMask|ShiftMask, XK_Right,  moveresizeedge, {.v = "R"} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ALTKEY,                XK_j,      aspectresize,   {.i = +24} },
	{ MODKEY|ALTKEY,                XK_k,      aspectresize,   {.i = -24} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY,                       XK_u, 	   shiftview,	   {.i = -1 } },
	{ MODKEY,                       XK_i,      shiftview,	   {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_u, 	   shifttag,	   {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_i,      shifttag,	   {.i = +1 } },
	{ MODKEY|ALTKEY,				XK_z,      movetoedge,	   {.v = "-1 1" } },
	{ MODKEY|ALTKEY,				XK_x,      movetoedge,	   {.v = "0 1" } },
	{ MODKEY|ALTKEY,				XK_c,      movetoedge,	   {.v = "1 1" } },
	{ MODKEY|ALTKEY,				XK_a,      movetoedge,	   {.v = "-1 0" } },
	{ MODKEY|ALTKEY,				XK_s,      movetoedge,	   {.v = "0 0" } },
	{ MODKEY|ALTKEY,				XK_d,      movetoedge,	   {.v = "1 0" } },
	{ MODKEY|ALTKEY,				XK_q,      movetoedge,	   {.v = "-1 -1" } },
	{ MODKEY|ALTKEY,				XK_w,      movetoedge,	   {.v = "0 -1" } },
	{ MODKEY|ALTKEY,				XK_e,  	   movetoedge,	   {.v = "1 -1" } },
	{ ControlMask|ShiftMask,        XK_Escape, spawn,          {.v = taskmancmd} },//opens htop
	{ MODKEY|ControlMask,      		XK_m,      spawn,          {.v = setmonitorcmd} },//sets monitor and wallpaper
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ ControlMask|MODKEY,           XK_Delete, spawn,          {.v = powercmd} },//shows power options
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = "kitty" } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

