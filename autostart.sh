#!bin/bash
#This script is to autostart my applications
#Wallpaper
exec xwallpaper --stretch /usr/share/backgrounds/slime.jpg &
sleep 1s
#Evolution - Calendar
exec evolution -c calendar &
exec /usr/lib/evolution-data-server/evolution-alarm-notify &
sleep 1s
#Megasync
exec megasync &
sleep 1s
#Pulse Audio
exec start-pulseaudio-x11 &
sleep 1s
#Gnome Keyring
exec /usr/bin/gnome-keyring-daemon --start --components=pkcs11 &
exec /usr/bin/gnome-keyring-daemon --start --components=secrets &
exec /usr/bin/gnome-keyring-daemon --start --components=ssh &
sleep 1s
#Cloud drives - unmount them the daemon will remount both
exec unmount_cloud_drives &
sleep 1s
